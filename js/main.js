$(document).ready(function(){
    $('.alert-info__btn').click(function (e) {
        e.preventDefault();

        $('.alert-info').fadeOut();
    });
    //tabs
    $('.tab-btn').click(function(e){
        e.preventDefault();

        var parents = $(this).parents('.tab-block');

        var $tab = $(this).data('tab');

        $(this).addClass('active').siblings('.tab-btn').removeClass('active');
        // $(this).closest('.tab-block').find('.tab-btn').not(this).removeClass('active')

        $(parents).children('.tab-item').removeClass('active');
        $($tab).addClass('active');

        var slider=$($tab).find('.slick-slider');

        if(slider.length) {
            slider[0].slick.slickGoTo(1);
        }
    });

    $('.u-info-tabs__link').click(function (e) {
        e.preventDefault();

        var tab = $(this).attr('href');

        $(this).addClass('u-info-tabs__link--active').parent().siblings('li').find('a').removeClass('u-info-tabs__link--active');
        $('.tab-u-item').removeClass('active');
        $(tab).addClass('active');
    });

    //

	$('.styler').styler();

    $('.cd-next').nextSection();

    //mobile nav btn
    $('#mob-btn').click(function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $('.header-nav').toggleClass('active');
    })

    $(document).click(function(event) {
        if ($(event.target).closest("#mob-btn, .header-nav").length) return;                 
        $('#mob-btn, .header-nav').removeClass('active');       
        event.stopPropagation();
    });


    $('.reviews__slider').slick({
        dots:false,
        arrows:true,
        prevArrow: '<a href="#" class="slick-prev slick-arrow"><span class="icon icon-arrow-left"></span></a>',
        nextArrow: '<a href="#" class="slick-next slick-arrow"><span class="icon icon-arrow-right"</span></a>',
        fade:true
    });

    $('.reviews__slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {


        $('.relevance__category-btn').eq(nextSlide).addClass('active').siblings('a').removeClass('active');
    });

    $('.relevance__category-btn').click(function (e) {
        e.preventDefault();
        var index  = $(this).index();

        $(this).addClass('active').siblings('a').removeClass('active');
        $('.reviews__slider').slick('slickGoTo', index);
    })

    $('.articles__slider').slick({
        dots:false,
        arrows:true,
        centerMode: true,
        slidesToShow:2,
        prevArrow: '<a href="#" class="slick-prev slick-arrow"><span class="icon icon-arrow-left"></span></a>',
        nextArrow: '<a href="#" class="slick-next slick-arrow"><span class="icon icon-arrow-right"</span></a>',
        autoplay: true,
        autoplaySpeed: 2000,

        responsive: [           
            {
              breakpoint: 1023,
                settings: {             
                    slidesToShow:2,  
                     centerMode: false,               
              }
            },
            {
              breakpoint: 767,
                settings: {             
                    slidesToShow:1,                 
              }
            },
        ]
    });


    // case tabs

    if(window.innerWidth <= 569) {
        $('.service-tab .case-btn,.advanced-tab .case-btn').each(function(){
            var tab = $(this).data('tab'),
                clone = $(tab).clone(),
                cloneLink =$(this).clone(),
                wrap = $(this).parents('.case-tabs-wrap');

            $(this).remove();
            $(cloneLink).appendTo(wrap);

            $(tab).remove();

            clone.insertAfter( cloneLink );
        });

        $('.case-btn').click(function () {
            var $this  =  $(this),
                parents = $this.parents('.case-tabs-wrap'),
                tab = $this.data('tab');


            $(parents).find('.case-btn').removeClass('active');

            $(this).addClass('active');

            $(tab).slideToggle(function () {
                var elementOffser = $this.offset();

                $("html:not(:animated),body:not(:animated)").animate({ scrollTop: elementOffser.top},1000);
            }).siblings('.case-tab').slideUp()


        });
    }else {
        $('.case-btn').click(function () {
            var parents = $(this).parents('.case-tabs-wrap')
            var tab = $(this).data('tab');

            $(parents).find('.case-btn,.case-tab').removeClass('active');

            $(this).addClass('active');
            $(tab).addClass('active');
        });
    }


    // set timeout  for animations  in header
    setTimeout(function(){
        $('.bn-animation__logo-shadow ').addClass('active');
    },1100);

    animationOpacity('.bn-a__line-left__circle,.bn-a__line-right__circle',1500);

    animationOpacity('.bn-a__line-left__heart',3000);
    animationOpacity('.bn-a__line-left__heart2',4000);
    animationOpacity('.bn-a__line-left__comment',5000);
    animationOpacity('.bn-a__line-left__comment2',6000);

    animationOpacity('.bn-a__line-right__heart-1',3000);
    animationOpacity('.bn-a__line-right__heart-2',4000);
    animationOpacity('.bn-a__line-right__heart-3',5000);
    animationOpacity('.bn-a__line-right__heart-4',6000);

    setTimeout(function(){
        $('.bn-animation__left-layers,.bn-animation__right-layers').addClass("active").delay(3000).queue(function(next){
            $(this).removeClass("active");
            next();
        });
    },5000);




    animationInterval('.bn-animation__left-layers',2000,10000);
    animationInterval('.bn-animation__left-layers',3000,12000);
    animationInterval('.bn-animation__left-layers',4000,15000);
    animationInterval('.bn-animation__left-layers',5000,18000);

    animationInterval('.bn-animation__right-layers',2000,10000);
    animationInterval('.bn-animation__right-layers',3000,13000);
    animationInterval('.bn-animation__right-layers',4000,15000);
    animationInterval('.bn-animation__right-layers',5000,15000);


    $("a.anchorLink").anchorAnimate();

    //  header

    $(window).scroll(function(){
        var scrollTop = $(window).scrollTop();

        if (scrollTop >  150 ) {
            $("#header").addClass('header__dark');
        }else {
            $("#header").removeClass('header__dark');
        }
    });

    // consult-form

    $('.consult-form__submit').click(function(e){
        e.preventDefault();

        $('.consult-form').addClass('send');
    });


    // turn off  zoom
    document.addEventListener('gesturestart', function (e) {
        e.preventDefault();
    });

    // packages

    $('.packages-item__btn-buy').click(function (e) {
        e.preventDefault();

        var parent = $(this).parents('.packages-item__content');

        parent.find('.packages-item__list-w').addClass('hide').removeClass('show');
        parent.find('.packages-item__subscription').removeClass('hide').addClass('show');
    });

    $('.packages-form-btn-pay').click(function (e) {
        e.preventDefault();
        var parent = $(this).parents('.packages-item__content');
        parent.find('.packages-item__list-w,.packages-item__subscription').addClass('hide').removeClass('show');
        parent.find('.packages-item__order').removeClass('hide').addClass('show');
    });

    $('.packages-item__back').click(function (e) {
        e.preventDefault();
        var parent = $(this).parents('.packages-item__content');
        parent.find('.packages-item__list-w').addClass('show').removeClass('hide');
        parent.find('.packages-item__subscription,.packages-item__order').removeClass('show').addClass('hide');
    });

    // packages-choice__btn
    $('.packages-choice__btn').click(function (e) {
        e.preventDefault();

        var choice = $(this).data('choice');

        $(this).parents('.packages-item__content').find('.packages-form__btn-sum').text(choice);
        $(this).parents('.packages-item__content').find('.packages-choice__btn').removeClass('packages-choice__btn--active');
        $(this).addClass('packages-choice__btn--active');
    });
});

function animationOpacity($name,$delay) {
    setTimeout(function(){
        $($name).css('opacity',1)
    },$delay);
}

function animationInterval($name,$delay,$delayInterval) {
    setTimeout(function(){
        setInterval(function () {
            $($name).addClass("active").delay(500).queue(function(next){
                $(this).removeClass("active");
                next();
            });
        },$delayInterval);
    },$delay);
}

//anchor scrolling animation
jQuery.fn.nextSection = function(settings) {
    settings = jQuery.extend({
        speed : 600
    }, settings);

    return this.each(function(){
        var caller = this
        $(caller).click(function (event) {
            event.preventDefault()
            var locationHref = window.location.href
            var elementClick = $(caller).parents('section').next('section');

            var destination = $(elementClick).offset().top;
            $("html:not(:animated),body:not(:animated)").animate({ scrollTop: destination}, settings.speed);
            return false;
        })
    })
}

jQuery.fn.anchorAnimate = function(settings) {

    settings = jQuery.extend({
        speed : 1100
    }, settings);

    return this.each(function(){
        var caller = this
        $(caller).click(function (event) {
            event.preventDefault()
            var locationHref = window.location.href
            var elementClick = $(caller).attr("href")

            var destination = $(elementClick).offset().top - parseInt(50);
            $("html:not(:animated),body:not(:animated)").animate({ scrollTop: destination}, settings.speed, function() {
                window.location.hash = elementClick
            });
            return false;
        })
    })
}

